package try_test

import "testing"

// 测试信息
// 测试类需要以 _test.go的方式结尾
// 测试方法需要以 Test开头表示
func TestFirstTry(t *testing.T) {
	t.Log("My first try!")
}
