package series

import "fmt"

// fibonacci serie的实现
func GetFibonacciSerie(n int) []int {
	ret := []int{1, 1}
	for i := 2; i < n; i++ {
		ret = append(ret, ret[i-2]+ret[i-1])
	}

	return ret
}

// 计算数字的平方
func Square(n int) int {
	return n * n
}

func init() {
	fmt.Println("init...")
}

func init() {
	fmt.Println("init2....")
}
