package groutine_test

import (
	"fmt"
	"testing"
	"time"
)

func TestGroutine(t *testing.T) {
	for i := 0; i < 10; i++ {

		// go在传递时采用的是值传递，每次指针都是不一样的
		go func(i int) {
			fmt.Println(i)
		}(i)

		// 这个地方是将i进行了共享, 这时就需要锁机制来保证
		go func() {
			fmt.Println(i)
		}()
	}
	time.Sleep(time.Millisecond * 50)
}
