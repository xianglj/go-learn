package obj_cache

import (
	"fmt"
	"sync"
	"testing"
)

func TestSyncPool(t *testing.T) {
	pool := &sync.Pool{
		New: func() interface{} {
			fmt.Println("Create a new object")
			return 100
		},
	}

	v := pool.Get().(int)
	fmt.Println(v)

	pool.Put(3)
	v1, _ := pool.Get().(int)
	fmt.Println(v1)
}

// sync.Pool的使用场景:
// 1. 通过复用，降低复杂对象的创建和GC代价
// 2. 携程安全，会有锁的开销
// 3  生命周期受GC影响，不适用于做链接池等，需自己管理生命周期的资源的池化
func TestSyncMultiPool(t *testing.T) {
	pool := &sync.Pool{
		New: func() interface{} {
			fmt.Println("Create a new object")
			return 100
		},
	}

	pool.Put(10)
	pool.Put(10)
	pool.Put(10)

	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(id int) {
			fmt.Println(pool.Get().(int))
			wg.Done()
		}(i)
	}
	wg.Wait()
}
