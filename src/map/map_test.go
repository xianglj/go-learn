package my_map

import "testing"

// 测试map的初始化方式
func TestInitMap(t *testing.T) {
	m1 := map[int]int{1: 1, 2: 4, 3: 9}
	t.Log(m1)

	t.Logf("len m1: %d", len(m1))
	// 访问某一个元素
	t.Log(m1[2])

	// 初始化一个空map
	m2 := map[int]int{}
	m2[4] = 16
	t.Logf("len m2: %d", len(m2))

	// 通过make初始化map
	m3 := make(map[int]int, 10)
	t.Logf("len m3: %d", len(m3))
}

// 对于map中不存在key的处理方式
func TestAccessNotExistingKey(t *testing.T) {
	m1 := map[int]int{}
	// 访问一个不存在的值, 会以0值展示，这是go的一个解决方案
	t.Log("不存在的key:", m1[1])

	m1[2] = 0
	t.Log(m1[2])

	// 将3的值赋值为0
	m1[3] = 0
	if v, ok := m1[3]; ok {
		t.Logf("key 3'value is : %d", v)
	} else {
		t.Log("key 3 is not existing.")
	}
}

// 遍历一个map
func TestTraveMap(t *testing.T) {
	m1 := map[int]int{1: 1, 2: 4, 3: 9}
	for k, v := range m1 {
		t.Log(k, v)
	}
}
