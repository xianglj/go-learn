package map_ext

import "testing"

func TestMapWithFunValue(t *testing.T) {
	m := map[int]func(op int) int{}

	m[1] = func(op int) int {
		return op
	}

	m[2] = func(op int) int {
		return op * op
	}

	m[3] = func(op int) int {
		return op * op * op
	}

	t.Log(m[1](2), m[2](2), m[3](2))
}

// Go内置集合中没有Set实现，可以map[type]bool
// 1. 元素唯一性
// 2. 基本操作
// 	1) 添加元素
//  2) 判断元素是否存在
//  3) 删除元素
//  4) 元素个数
func TestMapForSet(t *testing.T) {
	mySet := map[int]bool{}
	mySet[1] = true

	n := 1
	// 判断元素是否存在
	if mySet[n] {
		t.Logf("%d is existing..", n)
	} else {
		t.Logf("%d is not existing....", n)
	}

	mySet[3] = true
	// 获取长度
	t.Log(len(mySet))

	// 删除map中的元素, 指定需要删除的key
	delete(mySet, 1)

	// 判断元素是否存在
	if mySet[n] {
		t.Logf("%d is existing..", n)
	} else {
		t.Logf("%d is not existing....", n)
	}
}
