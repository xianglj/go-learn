package examples

import (
	"fmt"
	"log"
	"os"
)

// 对if控制结构进行测试
func IfControl(num int) {
	if num > 0 {
		fmt.Println("number is %d", num)
	}
}

// 验证break, continue等阻断流程
func IfBreakState(name string) (*os.File, error) {
	f, err := os.Open(name)
	if err != nil {
		return f, err
	}

	log.Println(f)
	return f, err
}

// for循环语句
func ForControl(num int) int {
	sum := 0
	if num > 0 {
		for i := 0; i < num; i++ {
			sum += i
		}
	}

	return sum
}

// 循环遍历string对象
func StringChar(str string) {
	for pos, char := range str {
		fmt.Printf("charactor %#U start at position %d \n", char, pos)
	}
}

// 翻转一个字符串
func ReverseStr(strin string) string {
	a := []rune(strin)
	for i, j := 0, len(a)-1; i < j; i, j = i+1, j-1 {
		a[i], a[j] = a[j], a[i]
	}

	return string(a)
}

// 将一个字符转换为16进制
func Unhex(c byte) byte {
	switch {
	case '0' <= c && c <= '9':
		return c - '0'
	case 'a' <= c && c <= 'f':
		return c - 'a' + 10
	case 'A' <= c && c <= 'F':
		return c - 'A' + 10
	}
	return 0
}

func SwitchCaseList(c byte) bool {
	switch c {
	case '=', '?', '+':
		return true
	}
	return false
}
