package fib

import (
	"testing"
)

func TestFibList(t *testing.T) {

	var (
		a = 1
		b = 1
	)

	// var a int = 1
	// var b int = 1

	t.Log(a, " ")
	for i := 0; i < 5; i++ {
		t.Log(" ", b)
		tmp := a
		a = b
		b = tmp + a
	}
}

// 交换两个变量的值
func TestExhange(t *testing.T) {
	a := 1
	b := 1
	tmp := a
	a = b
	b = a
	b = tmp

	a, b = b, a
	t.Log(a, b)
}
