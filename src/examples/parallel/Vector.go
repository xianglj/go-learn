package parallel

import "runtime"

type Vector []float64

var cpuNum = runtime.NumCPU()

func (v Vector) DoAll(u Vector) {
	c := make(chan int, cpuNum)
	for i := 0; i < cpuNum; i++ {
		go v.DoSome(i*len(v)/cpuNum, (i+1)*len(v)/cpuNum, u, c)
	}

	// drain the channel
	for i := 0; i < cpuNum; i++ {
		<-c // 等待任务的完成
	}
}

func (v Vector) DoSome(i, n int, u Vector, c chan int) {
	for ; i < n; i++ {
		v[i] += u.Op(v[i])
	}

	c <- 1 // send signal this piece is done
}

func (v Vector) Op(val float64) float64 {
	return val
}
