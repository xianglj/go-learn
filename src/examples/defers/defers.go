package defers

import "fmt"

// 进入
func trace(s string) string {
	fmt.Printf("entering: %s \n", s)
	return s
}

// 退出
func untrace(s string) {
	fmt.Printf("untrace: %s \n", s)
}

// 对trace方法的测试
func TraceTest(s string) {
	trace(s)
	// this func will run fater code finished
	defer untrace(s)
	fmt.Println("func finished")
}

func FunnyThing(s string) {
	b()
}

func un(s string) {
	fmt.Println("leaving: ", s)
}

func a() {
	defer un(trace("a"))
	fmt.Println("in a")
}

func b() {
	// 通过这一步, 可以看到, 虽然un是在结束后执行, 但是trace作为参数会先执行
	defer un(trace("b"))
	fmt.Println("in b")
	a()
}
