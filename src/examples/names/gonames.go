/**
该节主要用于对go中的名称定义的说明:
1. 如果名称是以大写开头, 代表该声明可以被其他的包访问
2. 如果是以小写开头, 则代表只能够在本包访问

包名的命名规范:
1. 包名都是以小写开头, 每个单词代表一个意思
*/
package names

import "fmt"

func PrintString(s string) {
	fmt.Println(s)
}
