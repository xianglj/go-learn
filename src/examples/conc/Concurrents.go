package conc

import (
	"examples/data/initialization"
	"fmt"
	"os"
	"sort"
	"time"
)

/*异步调用方法 */
func Announce(message string, delay time.Duration) {
	// 异步执行
	go func() {
		time.Sleep(delay)
		fmt.Println(message)
	}() // 立即执行该方法
}

/*对于channels的测试*/
func ForChannels() {
	ci := make(chan int)           // unbuffered channel of integer
	cj := make(chan int, 0)        // unbuffered channel of integer
	cs := make(chan *os.File, 100) // buffered channel of pointers for files

	_ = cj
	_ = cs

	list := initialization.Sequence([]int{1, 4, 6, 4, 2, 23})
	// 开启goroutine, 并等待sort执行完成
	go func() {
		sort.Sort(list)
		ci <- 1 // 发送一个信号, 并不关系具体的值
	}()

	// 等待c排序完成
	r, ok := <-ci

	fmt.Println("ForChannels: ", r, ok)
}
