/*该包是对allocation的方法进行学习, 并学习两个关键点: new 和 make的用法*/
package allocate

import (
	"bytes"
	"fmt"
	"sync"
)

/*定义个buffer对象*/
type SyncedBuffer struct {
	lock   sync.Mutex
	buffer bytes.Buffer
}

/*创建一个File对象*/
func NewFile(fd int, fileName string) *File {
	if fd < 0 {
		return nil
	}

	f := new(File)
	f.fd = fd
	f.name = fileName
	f.dirinfo = "nil"
	f.nepipe = 0
	return f
}

/*测试&表达式, 表示的是每次都返回一个新的对象*/
func NewFileLiteral(fd int, name string) *File {
	if fd < 0 {
		return nil
	}

	// 这种方式创建的对象, 必须保证每个字段按照顺序必须出现
	f := File{fd, name, "nill", 0}

	// 可以通过命令key:value的方式创建对象, 这样其他未出现的属性
	// 都将以默认的值出现
	f = File{fd: fd, name: name}

	// 如果在{}中没有出现任何的属性的指定, 那么改用法与 new(File)
	return &f
}

/*通过component方式创建多个*/
func NewCollectionLiteral() {
	// 创建一个数组
	const Enone int = 0
	const Eio int = 1
	const Einval int = 2

	// a := [...]string{Enone: "no error", Eio: "Eio", Einval: "invalid argument"}
	s := []string{Enone: "no error", Eio: "Eio", Einval: "invalid argument"}
	m := map[int]string{Enone: "no error", Eio: "Eio", Einval: "invalid argument"}

	for pos, value := range s {
		fmt.Printf("pos: %d, value: %s", pos, value)
	}

	// 遍历map
	for key, value := range m {
		fmt.Printf("key: %d, value: %s", key, value)
	}
}

/*file对象*/
type File struct {
	fd      int
	name    string
	dirinfo string
	nepipe  int
}
