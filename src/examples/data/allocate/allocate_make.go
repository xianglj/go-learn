package allocate

import "fmt"

/*通过make的方式创建对象, 该对象在创建的时候, 会自动的赋值*/
func NewAllocateMake() {
	// make返回的是一个具体的对象, 而不是内存地址
	var args []int = make([]int, 10, 100)
	for pos, val := range args {
		fmt.Printf("%d : %d \n", pos, val)
	}
}
