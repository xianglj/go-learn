/*对于输出格式化的实现*/
package out

import (
	"fmt"
	"log"
)

/*将结果以hex的形式输出*/
func FormatToHex(x uint64) {
	log.Printf("%d %x : %d %x \n", x, x, int64(x), int64(x))
}

/*按照原样输出内容*/
func OutOriginal(timeZone map[string]int) {
	log.Printf("%v \n", timeZone)
	log.Printf("%#v \n", timeZone)
}

/*比较输出之前的差异*/
func OutStruct(t *T) {
	// 只是输出对象中的值
	log.Printf("%v \n", t)
	// 食醋胡对一个的属性的名称
	log.Printf("%+v \n", t)
	// 将传入对象的所有信息都展示
	log.Printf("%#v \n", t)
}

/*输出t的实际类型*/
func PrintType(t *T) {
	log.Printf("%T \n", t)
}

type T struct {
	A int
	B float64
	C string
}

/*自定义输出格式*/
func (t *T) String() string {
	return fmt.Sprintf("%d/%g/%q", t.A, t.B, t.C)
}
