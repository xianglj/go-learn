/*该章节主要是对Append方法的介绍, 和使用*/
package operate

import "fmt"

/*主要是对数组新增元素进行设置*/
func AppendArray() {
	x := []int{1, 2, 4}
	x = append(x, 4, 5, 6)
	fmt.Println("result:", x)
}

/*通过...的方式将y的所有元素加入到x中*/
func AppendAllArray() {
	x := []int{1, 2, 3}
	y := []int{4, 5, 6}
	x = append(x, y...)
	fmt.Println("result: ", x)
}
