package types

import "fmt"

/*对于数组的应用*/
func Sum(a *[3]float64) (sum float64) {
	for _, value := range a {
		sum += value
	}

	return
}

// 比较两个数组对象的分配差别
func CompareArrays() {
	source := [10]int{1, 2, 3, 4, 5, 6}
	target := source
	// 更改target对象的数组
	target[7] = 12

	// 遍历连个数组
	fmt.Printf("source: ")
	for pos, value := range source {
		fmt.Printf("%d", value)
		if pos == len(source)-1 {
			fmt.Println("")
		}
	}

	fmt.Printf("target: ")
	for pos, value := range target {
		fmt.Printf("%d", value)
		if pos == len(source)-1 {
			fmt.Println("")
		}
	}
}
