/*对map类型的学习, 其中特殊的在于, slices不能作为map的key来使用*/
package types

import (
	"log"
)

var timeZone = map[string]int{
	"UTC": 0 * 60 * 60,
	"EST": -5 * 60 * 60,
	"CST": -6 * 60 * 60,
	"MST": -7 * 60 * 60,
	"PST": -8 * 60 * 60,
}

/*查看key所对应的值*/
func Get(key string) int {
	return timeZone[key]
}

/*判断map中是否包含key*/
func ContainsKey(key string) int {
	return timeZone[key]
}

/*通过多参数的形式, 判断是否获取key成功*/
func GetWithBool(key string) (int, bool) {
	var seconds int
	var ok bool
	seconds, ok = timeZone[key]
	return seconds, ok
}

/**获取key的值, 另外一种实现方式*/
func GetWithDefault(key string) int {
	if seconds, ok := timeZone[key]; ok {
		return seconds
	}

	log.Println("unkown time zone: ", key)
	return 0
}

/*判断key是否在map中存在*/
func IsPresent(key string) bool {
	_, present := timeZone[key]
	return present
}

/*从map中删除元素*/
func Delete(key string) {
	delete(timeZone, key)

	// 遍历元素, 查看是否删除成功
	for key, value := range timeZone {
		log.Printf("%s:%s \n", key, value)
	}
}

func TimeZone() map[string]int {
	return timeZone
}
