/*对于接口相关知识的学习*/
package initialization

import (
	"fmt"
	"sort"
)

type Sequence []int

// Methods by sort.Interfaces
func (s Sequence) Len() int {
	return len(s)
}

/*比较两个数字的大小*/
func (s Sequence) Less(i, j int) bool {
	return s[i] < s[j]
}

/*交换两个索引上的数字*/
func (s Sequence) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

/* 拷贝Sequence中的内容, 并返回新的Sequence对象*/
func (s Sequence) Copy() Sequence {
	sequences := make(Sequence, 0, len(s))
	return append(sequences, s...)
}

/*打印出其中的字符串, 在打印之前, 并对其中的元素进行排序*/
func (s Sequence) String() string {
	s = s.Copy()
	// 对结果执行排序
	sort.Sort(s)
	// str := "["

	// for i, ele := range s { // 该处的循环的时间复杂度是O(N^2)
	// 	if i > 0 {
	// 		str += " "
	// 	}
	// 	str += fmt.Sprint(ele)
	// }

	// return str + "]"

	// 我们可以直接使用Sprint的方式, 输出数组中值
	return fmt.Sprint([]int(s))
}
