package initialization

import "fmt"

type Stringer interface {
	String() string
}

var Value interface{}

func String() {
	// 该语法可以用来判断value的实际类型
	str, ok := Value.(string)
	if ok {
		fmt.Printf("string vlaue is %s \n", str)
	} else {
		fmt.Printf("value is not string\n")
	}
}
