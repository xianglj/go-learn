/*
	just a package comments
*/
package examples

/*定义一个枚举类*/
type T struct {
	name  string // name of object
	value int    // it vlaue
}

var name string

// getter方法
func Name() string {
	return name
}

// setter方法
func SetName(n string) {
	name = n
}
