package stringutil

import "fmt"

//表单信息的更改
func Reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}

	fmt.Printf("%s \n", s)
	return string(r)
}
