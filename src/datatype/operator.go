package main

import "fmt"

func main() {
	/*
		算数运算符: +, -, *, /, %, ++, --
			++： 给自己 + 1
			--： 给自己 - 1
	*/

	a := 10
	b := 3
	sum := a + b

	fmt.Printf("%d + %d = %d \n", a, b, sum)

	div := a / b
	fmt.Println(div)

	mod := a % b
	fmt.Println(mod)

	c := 3
	c++ // c + 1

	fmt.Println(c)

	c-- // c-1
	fmt.Println(c)

}
