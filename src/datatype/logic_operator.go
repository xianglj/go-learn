package main

import "fmt"

func main() {
	/*
		&&: 所有的操作数都为真，结果为真
		||: 所有操作数都为假, 则为假; 有一个为真则为真; "一真为真，全假为假"
	*/

	f1 := true
	f2 := false
	res1 := f1 && f2

	fmt.Println(res1)

	f3 := true
	res2 := f3 && f1
	fmt.Println(res2)

	fmt.Println(f1 || f2)
	fmt.Println(f2 || f2)

	fmt.Println(!false)
	fmt.Println(!true)

	a := 3
	b := 2
	c := 5

	res5 := a > b && c%a == b && a < (c/b)
	fmt.Println(res5)

	res6 := b*2 < c || a/b != 0 || c/a > b
	fmt.Println(res6)

	res7 := !(c/a == b)
	fmt.Println(res7)
}
