package main

import "fmt"

func main() {
	/*
		=: 把等号右边的值赋值给左边的变量
		+=: a += b, 相当于 a = a + b
	*/

	var a int
	a = 3
	fmt.Println(a)

	a += 4
	fmt.Println(a)

	a -= 3
	fmt.Println(a)

	a *= 2
	fmt.Println(a)

	a /= 3
	fmt.Println(a)

	a %= 1
	fmt.Println(a)
}
