package main

import "fmt"

func main() {
	/**
	Go语言的数据类型:
	1. 基本类型:
		布尔类型: bool
			取值: true/false
		数值类型
			整数: int8, int16, int32, int64, uint8, uint16, uint32, uint64
				有符号: 最高位表示符号位， 0 表示整数， 1表示复数，其余位为数值
					int8: (-128 ~ 127)
					int16: (-32768 ~ 32767)
					int32: (-2147483648 ~ 2147483648)
					int64: (-9223372036854775808 ~ 9223372036854775808)
				 无符号:
					uint8: (0 ~ 255)
					uint16: (0 ~ 65535)
					uint32: (0 ~ 4294967295)
					uint64: (0 ~ 18446744073709551615)

				uint8 == byte

				int: 这个根据计算机的位数进行推断，如果是32位则为int32, 如果为64位，则为int64
					但是从语法角度看, int 和int64不是同种类型

			浮点数:
			复数:
		字符串: string

	2. 符合数据类型
		array, slice, map, function, pointer, struct, interface, channel
	*/

	// 1. 布尔类型
	var b1 bool
	b1 = true
	fmt.Printf("%T, %t\n", b1, b1)

	b2 := false
	fmt.Printf("%T, %t\n", b2, b2)

	// 2. 整数
	var i1 int8
	i1 = 100

	fmt.Println(i1)

	var i2 uint8
	i2 = 200
	fmt.Println(i2)

	var i3 int
	i3 = 1000
	fmt.Println(i3)

	// 语法角度: int, int64不认为是同一种类型
	//var i4 int64
	//i4 = i3

	// uint8 和byte可以互相赋值
	var i5 uint8
	i5 = 100
	var i6 byte
	i6 = i5
	fmt.Println(i5, i6)

	var i7 = 100
	fmt.Printf("%T, %d \n", i7, i7)

	// 浮点
	var f1 float32
	f1 = 3.14
	var f2 float64
	f2 = 4.67
	fmt.Printf("%T, %.2f\n", f1, f1)
	fmt.Printf("%T, %.3f\n", f2, f2)

	var f3 = 2.55
	fmt.Printf("%T, %.2f\n", f3, f3)
}
