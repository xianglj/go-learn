package main

import "fmt"

func main() {
	/*
		位运算符:
			将数值，转为二进制后，按位操作
		按位&:
			对应数据都为1，则为1; 有一个为0则为0
		按位|:
			对应数据都为0才为0; 有一个为1则为1
		异或^:
			二元: a^b
				对应的值不同为1,相同为0
			一元:
				按位取反
				1-->0
				0-->1
		位清空: &^
			对于a&^b
			对于b上的每个数值,
			如果为0, 则取a对应位上的数值
			如果为1, 则结果位取零

		<<: 按位 左移动, 将数值转换为二进制，向左移动位置
		>>: 按位右移, 将数值转换为二进制，向右移动响应位置
	*/

	a := 60
	b := 13

	/*
		a: 60 0011 1100
		b: 13 0000 1101
		&: 12 0000 1100
		|:    0011 1101
		^:    0011 0001 // 不同为1, 相同为0
		&^:	  0011 0000 // 为0取a上，为1去0
	*/
	fmt.Printf("a:%d, %b \n", a, a)
	fmt.Printf("b:%d, %b \n", b, b)
	fmt.Printf("a&b:%d, %b \n", (a & b), (a & b))

	res2 := a | b
	fmt.Printf("a | b:%d, %b \n", res2, res2)

	res3 := a ^ b
	fmt.Printf("a ^ b:%d, %b \n", res3, res3)

	res5 := a &^ b
	fmt.Printf("a &^ b:%d, %b \n", res5, res5)

	res6 := ^a
	fmt.Printf("^a:%d, %b \n", res6, res6)

	c := 8
	res7 := c << 2
	fmt.Printf("a<<2:%d, %b \n", res7, res7)

	res9 := c >> 2
	fmt.Printf("a>>2:%d, %b \n", res9, res9)
}
