package main

import "fmt"

func main() {
	/*
		字符串：
			1. 概念： 多个byte的集合, 理解为字符序列
			2. 语法：使用双引号
					"abc", "hello", "A"
					也可以使用``
			3. 编码问题:
				ASCII(没过标准信息交换码)
				Unicode编码表: 号称同意了全世界
					UTF-8, UTF-16, UTF-32...
			4. 转义字符:
				A: 有一些字符，有特殊的作用，可以转移为特殊字符
				B: 有一些字符, 就是一个普通的字符，加上转义就有特殊含义
					\n\t
	*/

	// 1. 定义字符串
	var s1 string
	s1 = "wang 'er' you"
	fmt.Printf("%T, %s\n", s1, s1)

	s2 := `Hello World`
	fmt.Printf("%T, %s\n", s2, s2)

	// 2. 区别: 'A', "A"
	v1 := 'A'
	v2 := "A"

	fmt.Printf("%T, %d\n", v1, v1)
	fmt.Printf("%T, %s\n", v1, v2)

	v3 := '中'
	fmt.Printf("%T, %d, %c, %q\n", v3, v3, v3, v3)

	// 转义字符
	fmt.Println("\"Hello World\"")
	fmt.Println(`"Hello" World`)
}
