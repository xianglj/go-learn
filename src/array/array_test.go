package array_test

import "testing"

func TestArrayInit(t *testing.T) {
	var arr [3]int
	arr1 := [4]int{1, 2, 3, 4}

	// 通过 ...的方式自动计算数组的长度
	arr3 := [...]int{1, 3, 4, 5}
	arr1[1] = 5
	t.Log(arr[1], arr[2])
	t.Log(arr1, arr3)
}

// 测试数组的遍历方式
func TestArrayTravel(t *testing.T) {
	arr3 := [...]int{1, 2, 3, 4}

	// 通过索引的方式遍历
	for i := 0; i < len(arr3); i++ {
		t.Log(arr3[i])
	}

	// 如果我们并不关系idx是什么，可以通过_来占位，表示接受
	// 对应的数据信息
	for _, e := range arr3 {
		t.Log(e)
	}
}

// 测试数组元素的截取
func TestArraySection(t *testing.T) {
	arr3 := [...]int{1, 2, 3, 4, 5}
	// 取前三个元素
	arr3_sec := arr3[:3]
	t.Log(arr3_sec)

	// go语言不支持负数取值
	//arr4_sec := arr3[:-1]
}
