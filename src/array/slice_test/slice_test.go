package slice_test

import "testing"

/*
切片内部结构:
ptr: *Elem，存储的是一个数组的指针引用
len: 元素的个数
cap: 内部数组的容量*/
func TestSliceInit(t *testing.T) {
	// 余普通数组相比，没有指定数组的任何长度
	var s0 []int

	t.Log(len(s0), cap(s0))

	// 加入一个元素
	s0 = append(s0, 1)
	t.Log(len(s0), cap(s0))

	s1 := []int{1, 2, 3, 5}
	t.Log(len(s1), cap(s1))

	// 通过make创建slice
	s2 := make([]int, 3, 5)
	t.Log(len(s2), cap(s2))
	// 无法访问s[3]以及之后的元素，不能访问长度之外的元素信息
	t.Log(s2[0], s2[1], s2[2])

	s2 = append(s2, 1)
	t.Log(s2[0], s2[1], s2[2], s2[3])
}

// 对于slice的数据增长，增长的cap为2^n的计算方式
func TestSliceGrowing(t *testing.T) {
	s := []int{}
	for i := 0; i < 10; i++ {
		// 这个能够是的slice能够是的长度自增长，并且
		// 在发生扩容的时候，会将数据拷贝到新的slice数组
		s = append(s, i)
		t.Log(len(s), cap(s))
	}
}

// slice共享内存实现
func TestSliceShareMemory(t *testing.T) {
	years := []string{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
		"Aug", "Sep", "Oct", "Nov", "Dec"}
	Q2 := years[3:6]
	t.Log(Q2, len(Q2), cap(Q2))
	summer := years[5:8]
	t.Log(summer, len(summer), cap(summer))

	// 修改summer的值
	summer[0] = "Unkown"
	t.Log(Q2)
}

func TestSliceComparing(t *testing.T) {
	a := []int{1, 2, 3, 4}
	b := []int{1, 2, 3, 4}
	t.Log(a, b)
	// if a == b { // slice can only be compared to nil
	// 	t.Log("Equals")
	// }
}
