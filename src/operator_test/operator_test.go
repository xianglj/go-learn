package operator_test

import "testing"

const (
	Readable = 1 << iota
	Writable
	Executable
)

func TestComapreArray(t *testing.T) {
	a := [...]int{1, 2, 3, 4}
	b := [...]int{1, 2, 3, 4}
	d := [...]int{1, 3, 4, 5}
	// c := [...]int{1, 2, 3, 4, 5}

	t.Log(a == b)
	// 对于数组长度不一致的情况，也会会导致编译错误
	//t.Log(a == c)
	t.Log(a == d)

}

func TestBigClear(t *testing.T) {
	// 操作位: &^, 如果右边的位上为1，无论左边是什么都是0；如果左边的位上为1，则右边位保持原数字
	a := 7
	a = a &^ Writable
	t.Log(a&Readable == Readable, a&Writable == Writable, a&Executable == Executable)
}
