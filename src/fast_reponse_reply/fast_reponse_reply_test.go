package fast_resp_reply

import (
	"fmt"
	"runtime"
	"testing"
	"time"
)

func runTask(id int) string {
	time.Sleep(10 * time.Microsecond)
	return fmt.Sprintf("The result is from %d", id)
}

// 返回数据
func FirstResponse() string {
	numOfRunner := 10
	// buffered channel可以实现与channel解耦关系
	ch := make(chan string, numOfRunner)
	for i := 0; i < numOfRunner; i++ {
		go func(i int) {
			ret := runTask(i)
			ch <- ret
		}(i)
	}

	return <-ch
}

// 返回数据
func AllResponse() string {
	numOfRunner := 10
	// buffered channel可以实现与channel解耦关系
	ch := make(chan string, numOfRunner)
	for i := 0; i < numOfRunner; i++ {
		go func(i int) {
			ret := runTask(i)
			ch <- ret
		}(i)
	}

	finalResult := ""
	for j := 0; j < numOfRunner; j++ {
		finalResult += <-ch + "\n"
	}
	return finalResult
}

func TestFirstResponse(t *testing.T) {
	t.Log("tesg")
	t.Log("Before:", runtime.NumGoroutine())
	t.Log(FirstResponse())
	t.Log(AllResponse())
	t.Log("After:", runtime.NumGoroutine())
}
