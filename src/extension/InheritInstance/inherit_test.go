package InheritInstance

import (
	"fmt"
	"testing"
)

// custome type: 实际为string类型的别名
type Code string

type Programmer interface {
	WriteHelloWorld() Code
}

type GoProgrammer struct {
}

func (p *GoProgrammer) WriteHelloWorld() Code {
	return "Go Programmer Hello World"
}

type JavaProgrammer struct {
}

func (j *JavaProgrammer) WriteHelloWorld() Code {
	return "Java Programmer Hello World"
}

// Programmer是一个接口，因此只能传入一个指针
func writeFirstProgram(p Programmer) {
	// %T 输出指针类型
	fmt.Printf("%T %v \n", p, p.WriteHelloWorld())
}

func TestPolymorphism(t *testing.T) {
	// goProg := new(GoProgrammer)
	// 通过GoProgrammer{}方式初始化, 则需要转换为指针
	goProg := &GoProgrammer{}
	javaProg := new(JavaProgrammer)

	writeFirstProgram(goProg)
	writeFirstProgram(javaProg)
}
