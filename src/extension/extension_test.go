package extension

import (
	"fmt"
	"testing"
)

type Pet struct {
}

func (p *Pet) Speak() {
	fmt.Println(".....")
}

func (p *Pet) SpeakTo(host string) {
	p.Speak()
	fmt.Println("  ", host)
}

// 这种方式可以直接调用Pet的方法
type Dog struct {
	Pet
}

// 虽然Dog也定义了该方法，但是调用该方法的时候,
// 父类的方法一样的会被执行
func (d *Dog) Speak() {
	fmt.Println("Wang!")
}

func TestDog(t *testing.T) {
	var dog *Dog = new(Dog)
	dog.SpeakTo("Chao")
}
