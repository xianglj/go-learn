package extension

import (
	"fmt"
	"testing"
)

// 通过断言的方式判断对应参数的类型
func DoSomething(p interface{}) {
	if i, ok := p.(int); ok {
		fmt.Println("Integer", i)
		return
	}

	if i, ok := p.(string); ok {
		fmt.Println("string", i)
		return
	}

	fmt.Println("Unkown Tyupe")
}

// 通过断言的方式判断传入参数的类型
func DoSomethingWithSwitch(p interface{}) {
	switch v := p.(type) {
	case int:
		fmt.Println("Integer", v)
	case string:
		fmt.Println("string", v)
	default:
		fmt.Println("Unkown Type")
	}
}

func TestDoSomething(t *testing.T) {
	DoSomething(1)
	DoSomething("Hello World")
	DoSomethingWithSwitch(23)
}
