package encap_test

import (
	"fmt"
	"testing"
	"unsafe"
)

type Employee struct {
	Id   string
	Name string
	Age  int
}

// 这种方式直接传入指针，减少了内存的开销
func (e *Employee) String() string {
	// 打印出地址信息
	fmt.Printf("Address is %x \n", unsafe.Pointer(&e.Name))
	return fmt.Sprintf("ID:%s/Name:%s/Age:%d", e.Id, e.Name, e.Age)
}

// 这种写法，实际上对对象结构的复制，
// 因此会导致数据的复制，和多余内存的开销
func (e Employee) EString() string {
	// 打印出地址信息
	fmt.Printf("Address is %x \n", unsafe.Pointer(&e.Name))
	return fmt.Sprintf("ID:%s-Name:%s-Age:%d", e.Id, e.Name, e.Age)
}

func TestStructOperations(t *testing.T) {
	e := Employee{"0", "Bob", 20}
	fmt.Printf("Address is %x \n", unsafe.Pointer(&e.Name))
	t.Log(e.String())
	t.Log(e.EString())
}
