package obj_pool

import (
	"errors"
	"fmt"
	"testing"
	"time"
)

// 重复使用对象
type ReusableObj struct {
}

// 对象池
type ObjectPool struct {
	bufChan chan *ReusableObj
}

// 创建对象池
func NewObjectPool(numOfObj int) *ObjectPool {
	objPool := ObjectPool{}
	objPool.bufChan = make(chan *ReusableObj, numOfObj)
	for i := 0; i < numOfObj; i++ {
		objPool.bufChan <- &ReusableObj{}
	}

	return &objPool
}

// 获取对象
func (p *ObjectPool) GetObj(timeout time.Duration) (*ReusableObj, error) {
	select {
	case ret := <-p.bufChan:
		return ret, nil
	case <-time.After(timeout):
		return nil, errors.New("time out")
	}
}

func (p *ObjectPool) ReleaseObj(obj *ReusableObj) error {
	select {
	case p.bufChan <- obj:
		return nil
	default:
		return errors.New("overflow")
	}
}

func TestObjPool(t *testing.T) {
	pool := NewObjectPool(10)
	for i := 0; i < 12; i++ {
		if v, err := pool.GetObj(time.Second * 1); err != nil {
			t.Error(err)
		} else {
			fmt.Printf("%T\n", v)
			// if err := pool.ReleaseObj(v); err != nil {
			// 	t.Error(err)
			// }
		}
		fmt.Println("Done")
	}
}
