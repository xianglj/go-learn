package channel

import (
	"fmt"
	"testing"
	"time"
)

func TestCancel(t *testing.T) {
	cancelChan := make(chan struct{}, 0)
	for i := 0; i < 5; i++ {
		go func(i int, cancelChan chan struct{}) {
			for {
				if isCanceled(cancelChan) {
					break
				}

				time.Sleep(time.Millisecond * 5)
			}
			fmt.Println(i, "Done")
		}(i, cancelChan)
	}

	cancel_2(cancelChan)
	time.Sleep(time.Second * 1)
}

// 判断任务是否已经取消
func isCanceled(ch chan struct{}) bool {
	select {
	case <-ch:
		return true
	default:
		return false
	}
}

// 写出数据
func cancel_1(ch chan struct{}) {
	ch <- struct{}{}
}

// 关闭channel, 实现广播
func cancel_2(cancelChan chan struct{}) {
	close(cancelChan)
}
