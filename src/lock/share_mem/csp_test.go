package share_mem

import (
	"fmt"
	"testing"
	"time"
)

// service服务类
func service() string {
	fmt.Println("...service")
	time.Sleep(500 * time.Millisecond)
	return "hello service"
}

// 执行service
func doService() chan string {
	// var strChan = make(chan string)
	// 通过buffered chan让通信不再等待，可以直接向下执行
	var strChan = make(chan string, 1)
	go func() {
		res := service()
		// 写出结果
		strChan <- res
		// 输出
		fmt.Println("service exit")
	}()

	return strChan
}

// 执行其他任务
func doOther() {
	time.Sleep(time.Millisecond * 500)
	fmt.Println("other service")
}

func TestCsp(t *testing.T) {
	var strChan = doService()
	doOther()
	fmt.Println(<-strChan)
	fmt.Println("exit")
}
