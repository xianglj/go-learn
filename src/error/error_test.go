package error

import (
	"errors"
	"fmt"
	"testing"
)

// 错误定义
var LessThanTwoError = errors.New("n shold be not less than 2")

// 错误定义
var LargerThenHundredError = errors.New("n shold be not larger than 100")

func GetFibonacci(n int) ([]int, error) {
	// if n < 0 || n > 100 {
	// 	return nil, errors.New("n shold be in [0, 100]")
	// }

	if n < 2 {
		return nil, LessThanTwoError
	}

	if n > 100 {
		return nil, LargerThenHundredError
	}

	fibList := []int{1, 1}
	for i := 2; i < n; i++ {
		fibList = append(fibList, fibList[i-2]+fibList[i-1])
	}

	return fibList, nil
}

func TestFibonacci(t *testing.T) {
	if v, err := GetFibonacci(-10); err != nil {
		if err == LessThanTwoError {
			fmt.Println("It is less.")
		}
		if err == LargerThenHundredError {
			fmt.Println("It is larger")
		}
		t.Log(err)
	} else {
		t.Log(v)
	}
}

// os.Exit()函数退出的时候，不会执行defer函数
func TestPanicVxExit(t *testing.T) {

	defer func() {
		fmt.Println("Finally!")

		// recover能够恢复函数的执行
		// recover并不检测发生具体错误，可能导致错误被忽略，
		// 强制recover之后，也可能导致整个程序还是无法运行。
		if err := recover(); err != nil {
			fmt.Println("recovered from ", err)
		}
	}()

	fmt.Println("start")
	panic(errors.New("Something wrong!"))
	// os.Exit(-1)
}
