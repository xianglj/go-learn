package reflect_test

import (
	"fmt"
	"reflect"
	"testing"
)

// 检验传入的值类型
func CheckType(v interface{}) {
	t := reflect.TypeOf(v)
	fmt.Println("type:", t)

	// 通过kind判断类型
	switch t.Kind() {
	case reflect.Float32, reflect.Float64:
		fmt.Println("Float")
	case reflect.Int, reflect.Int32, reflect.Int64:
		fmt.Println("Integer")
	default:
		fmt.Println("Unkown", t)
	}
}

func TestTypeAndValue(t *testing.T) {
	var f int64 = 10
	t.Log(reflect.TypeOf(f), reflect.ValueOf(f))
	t.Log(reflect.ValueOf(f).Type())
}

func TestBasicType(t *testing.T) {
	var f *float64 = new(float64)
	CheckType(f)
}

type Employee struct {
	EmployeeID string
	Name       string `format: "normal"`
	Age        int
}

func (e *Employee) UpdateAge(newVal int) {
	e.Age = newVal
}

type Customer struct {
	CookieID string
	Name     string
	Age      int
}

func TestInvokeByName(t *testing.T) {
	e := &Employee{"1", "Mike", 30}
	fieldName := "Name"
	t.Logf("Name: value(%[1]v), Type(%[1]T)", reflect.ValueOf(*e).FieldByName(fieldName))
	if nameField, ok := reflect.TypeOf(*e).FieldByName(fieldName); !ok {
		t.Error("Failed to get 'Name' field")
	} else {
		t.Log("Tag: format", nameField.Tag.Get("format"))
	}
	reflect.ValueOf(e).MethodByName("UpdateAge").
		Call([]reflect.Value{reflect.ValueOf(1)})
	t.Log("Updated Age:", e)
}
