package main

import (
	"examples"
	"examples/conc"
	"examples/data/allocate"
	"examples/data/initialization"
	"examples/data/operate"
	"examples/data/out"
	"examples/data/types"
	"examples/defers"
	"fmt"
	"os"
	"time"
)

func main() {

	// 异步调用
	conc.Announce("33333", time.Duration(1))

	// for control
	sum := examples.ForControl(12)
	fmt.Printf("sum is %d", sum)

	// 遍历字符串
	examples.StringChar("hello go!")

	// 翻转一个字符串
	reverseStr := examples.ReverseStr("上海自来水来自海上")
	fmt.Println(reverseStr)

	// 将一个字符转换为16进制
	fmt.Println(examples.Unhex(66))
	fmt.Println(examples.SwitchCaseList('='))

	defers.TraceTest("dddd")

	// funny code
	defers.FunnyThing("s")

	// memory allocate
	// p := new(allocate.SyncedBuffer) // allocate memory, but all value is zero

	// declare a variable, ready to use
	// var v allocate.SyncedBuffer

	// 测试通过new创建对象
	allocate.NewCollectionLiteral()

	allocate.NewAllocateMake()

	array := [...]float64{7.0, 8.5, 9.1}
	// 这种方式在go中不被建议, 建议采用slice的方式进行处理
	x := types.Sum(&array) // note the explicit address-of operator
	fmt.Printf("sum result is %f \n", x)

	// 比较实用make的差异
	types.CompareArrays()

	// 对于map数据类型的实现
	offset := types.Get("EST")
	fmt.Printf("%s value is %d \n", "EST", offset)

	// 判断map中是否包含了key
	fmt.Printf("map contains key '%s', value is '%d' \n", "est", types.ContainsKey("est"))

	// 通过多参数的形式, 判断对应的key是否包含
	seconds, ok := types.GetWithBool("est")
	fmt.Printf("get %s from map , result is %s , v: %d \n", "est", ok, seconds)

	seconds, ok = types.GetWithBool("EST")
	fmt.Printf("get %s from map , result is %s , v: %d \n", "est", ok, seconds)

	// 对于获取map中的值的另一种写法
	seconds = types.GetWithDefault("ST")
	fmt.Printf("%s time zone seconds is %d \n", "ST", seconds)

	// 判断key是否已在map中存在
	present := types.IsPresent("EST")
	fmt.Println("EST is present? ", present)

	// 删除key
	types.Delete("EST")

	// 格式化输出
	out.FormatToHex(1<<64 - 1)
	out.OutOriginal(types.TimeZone())
	t := &out.T{10, -2.35, "Hello\tdef"}

	out.OutStruct(t)

	// 输出对象的实际类型
	out.PrintType(t)

	// 对Append方法的测试
	operate.AppendArray()
	operate.AppendAllArray()

	// 测试常量的使用
	e := initialization.ByteSize(1e13)
	fmt.Println("byteSize: ", e)

	// 测试一下value.的用法
	seq := initialization.Sequence([]int{1, 2, 4, 6, 7, 43, 23, 45, 16})
	// 输出
	fmt.Println(seq)

	stringer := new(initialization.Stringer)
	initialization.Value = stringer

	// 输出值
	initialization.String()
	initialization.Value = "ddd"
	initialization.String()

	// 对于channels的使用
	conc.ForChannels()

	// 用于获取命令行参数
	if len(os.Args) > 1 {
		fmt.Println("hello world", os.Args[1])
	}
}
