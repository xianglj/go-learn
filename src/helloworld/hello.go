package main

import "fmt"

// main 函数必须在main的包下
func main() {
	fmt.Println("Hello world")
	fmt.Print("Go Go Go!!")
	fmt.Println("你好, GO")
}
