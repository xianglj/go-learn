package unit

// 对于单元测试来讲，使用下划线包名会有问题
import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

// 内置单元测试框架
// Fail, Error: 该测试失败，该测试继续，其他测试集训执行
// FailNow, Fatal: 该测试失败， 该测试终止，其他测试继续执行
func TestSquare(t *testing.T) {
	inputs := [...]int{1, 2, 3}
	expected := [...]int{1, 5, 9}

	for i := 0; i < len(inputs); i++ {
		ret := square(inputs[i])
		assert.Equal(t, expected[i], ret, "结果不匹配")
		if ret != expected[i] {
			t.Errorf("Input is %d, the expected is %d, the actual is %d", inputs[i], expected[i], ret)
		}
	}
}

// 断言的使用：https://github.com/stretchr/testify
func TestErrorInCode(t *testing.T) {
	fmt.Println("Start")
	t.Error("Error")
	fmt.Println("End")
}

func TestFatalInCode(t *testing.T) {
	fmt.Println("Start")
	t.Fatal("Error")
	fmt.Println("End")
}
