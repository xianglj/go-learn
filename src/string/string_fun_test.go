package string_test

import (
	"strconv"
	"strings"
	"testing"
)

func TestStringFn(t *testing.T) {
	s := "A,B,C"
	parts := strings.Split(s, ",")
	for _, part := range parts {
		t.Log(part)
	}

	// 字符串链接
	t.Log(strings.Join(parts, "#"))
}

func TestStringConv(t *testing.T) {
	// 将int转换为字符串操作
	s := strconv.Itoa(10)
	t.Log("str" + s)

	// 字符串转换为整形
	if i, err := strconv.Atoi("10"); err == nil {
		t.Log(10 + i)
	}

}
