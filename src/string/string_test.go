package string_test

import "testing"

func TestString(t *testing.T) {
	var s string
	t.Log(s)
	t.Log(len(s))

	// 开始复制
	s = "hello"
	t.Log(len(s))

	// 因为不可变, 所以这个地方不能更改
	// s[1] = '3' // string是不可变的byte slice

	// s = "\xE4\xB8\xA5" // 可以存储任何二进制数据
	s = "\xE4\xB8\xA5\xFF" // 可以存储任何二进制数据
	t.Log(s)
	t.Log(len(s))

	// 重新赋值
	s = "中"
	t.Log(len(s)) // 输出byte的长度

	// rune用法：取出字符串中unicode, 是一个rune的slice
	c := []rune(s)
	t.Log(len(c))

	t.Logf("中 unicode %x", c[0])
	t.Logf("中 UTF8 %x", s)
}

func TestStringToRune(t *testing.T) {
	s := "中华人名共和国"
	for _, c := range s {
		t.Logf("%[1]c %[1]d", c) // 其中的1, 表示都以第一个参数对应
	}
}
