package fn_test

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

// 测试函数多返回值
func returnMultiValues() (int, int) {
	return rand.Intn(10), rand.Intn(20)
}

// 统计一个方法执行用时
func timeSpent(inner func(op int) int) func(op int) int {
	return func(n int) int {
		start := time.Now()
		ret := inner(n)
		fmt.Println("time spent:", time.Since(start).Seconds())
		return ret
	}
}

func slowFun(op int) int {
	time.Sleep(time.Second * 1)
	return op
}

// 可变参数的使用
func Sum(ops ...int) int {
	ret := 0
	for _, val := range ops {
		ret = ret + val
	}

	return ret
}

func Clear() {
	fmt.Println("Clear Resources.")
}

func TestDefer(t *testing.T) {
	// defer 的用法，在方执行完成，退出时执行
	defer Clear()

	fmt.Println("Start")

	// 执行panic，跑出一个不可修复的异常
	panic("Fatal Error")
}

// 可变长参数的学习
func TestVarParams(t *testing.T) {
	t.Log(Sum(1, 2, 3, 4))
	t.Log(Sum(1, 2, 3, 4, 5))
}

func TestFn(t *testing.T) {
	a, b := returnMultiValues()
	t.Log(a, b)

	// 测试方法调用
	tsSF := timeSpent(slowFun)
	t.Log(tsSF(10))
}
