package main

import "fmt"

func main() {
	/*
		变量: variable
		概念: 一小块内存, 用于存储数据, 在程序运行中数值可以改变
		使用:
			step1: 变量的声明, 也叫做定义
			step2: 变量的访问, 赋值和取值

		go的特性：
			静态语言： 强类型语言
				go, java, c++, c##
			动态语言: 弱类型语言
				javascript, php, python, ruby...
	*/
	// 第一种: 定义变电，然后赋值
	var num1 int
	num1 = 38
	fmt.Printf("num1的数值是： %d \n", num1)

	// 卸载一行
	var num2 int = 15
	fmt.Printf("num2的数值是: %d\n", num2)

	// 第二种: 弱类腿短
	var name = "王小二"
	fmt.Printf("类型是：%T， 数值是: %s\n", name, name)

	// 第三种, 简短定义
	sum := 100
	fmt.Println(sum)

	var a, b, c int
	a = 1
	b = 2
	c = 3
	fmt.Println(a, b, c)

	var f1, f2, f3 = 199, 3.23, "Go"
	fmt.Println(f1, f2, f3)

	var (
		studentName = "李小花"
		age         = 18
		set         = "女"
	)
	fmt.Printf("学生姓名: %s, 学生年龄: %d, 学生性别: %s", studentName, age, set)
}
