package main

import "fmt"

/*
1. 变量定义之后才能够使用
2. 变量的类型和赋值必须一致
3. 同一个作用于内，变量名不能相同
4. 简短定义方式, 左边的变量至少有一个是新的
5. 简短定义方式, 不能用来定有全局变量
6. 变量的灵芝, 则是默认是：
	整形: 默认值是0
    字符串：默认值时""
	浮点数: 默认也是0
*/

var a = 100 //全局变量
var b int = 2000

// c:= 3000 // 语法错误
func main() {
	var num int
	num = 100
	fmt.Printf("num的数值是：%d, 地址是:%p\n", num, &num)

	num = 200
	fmt.Printf("num的数值是：%d, 地址是:%p\n", num, &num)

	var name string
	//name = 100  # cannot use 100 as type string in assignment
	name = "王小二"
	fmt.Println(name)

	num, name, sex := 1000, "李小华", "女"
	fmt.Println(num, name, sex)

	fmt.Println("-------------------")
	var n int // 整数值, 默认值是0
	fmt.Println(n)
	var f float64
	fmt.Println(f)

	var s string
	fmt.Println(s)
}
