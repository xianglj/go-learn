package main

import "fmt"

func main() {
	const (
		a = iota   // 0
		b          //1
		c          //2
		d = "haha" // haha iota = 3
		e          // haha // haha iota = 4
		f = 100    // 100 iota = 5
		g          // 100 iota = 6
		h = iota   // 7
		i          // iota 8
	)

	fmt.Println(a, b, c, d, e, f, g, h, i)
}
