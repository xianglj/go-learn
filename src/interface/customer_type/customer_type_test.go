package customer_type

import (
	"fmt"
	"testing"
	"time"
)

// 这种方式相当于给行数启用了一个别名
type IntConv func(op int) int

// 统计一个方法执行用时
func timeSpent(inner IntConv) IntConv {
	return func(n int) int {
		start := time.Now()
		ret := inner(n)
		fmt.Println("time spent:", time.Since(start).Seconds())
		return ret
	}
}

func slowFun(op int) int {
	time.Sleep(time.Second * 1)
	return op
}

func TestTimeSpent(t *testing.T) {
	tsSF := timeSpent(slowFun)
	t.Log(tsSF(15))
}
