package interface_test

import "testing"

// 定义一个接口
type Programmer interface {
	WriteHelloWorld() string
}

// 实现一个实现类： Duck Type
type GoProgrammer struct {
}

// 保持方法签名一直，就表示了实现了接口
// 对接口是没有强依赖的
func (gp *GoProgrammer) WriteHelloWorld() string {
	return "fmt.Println(\"Hello World\")"
}

func TestClient(t *testing.T) {
	var p Programmer
	p = new(GoProgrammer)
	t.Log(p.WriteHelloWorld())
}
